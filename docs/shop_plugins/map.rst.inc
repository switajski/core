:doc:`/shop_plugins/allgemein`
  Hier finden Sie Informationen über das Plugin-System

:doc:`/shop_plugins/aufbau`
  Hier finden Sie Informationen zur Ordner- und Dateistruktur sowie zum Aufbau der info.xml.

:doc:`/shop_plugins/bootstrapping`
  Hier finden Sie Informationen zum Bootstrapping-Prozess im JTL-Shop 5+.

:doc:`/shop_plugins/variablen`
  Hier finden Sie Informationen zu Plugin-Variablen.

:doc:`/shop_plugins/hooks`
  Allgemeines über Hooks im JTL-Shop.

:doc:`/shop_plugins/hinweise`
    Allgemeine Hinweise, Tipps und Tricks zur Entwicklung von Plugins, insbesondere für Shop 4+.

:doc:`/shop_plugins/cache`
  Der Umgang mit dem Objektcache im JTL-Shop.

:doc:`/shop_plugins/mailing`
  Versenden von Emails.

:doc:`/shop_plugins/hook_list`
  Die einzelnen Hooks des JTL-Shops.

:doc:`/shop_plugins/fehlercodes`
  Mögliche Fehlercodes bei Plugininstallation

:doc:`/shop_plugins/short_checkout`
  Welche Änderungen gibt es im Zusammenhang mit dem verkürzten Checkout ab Version 4.06.

:doc:`/shop_plugins/sicherheit`
  Anleitung für die Programmierung von sicheren Plugins im JTL-Shop.
