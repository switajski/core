JTL-Shop-Template Evo
=====================

.. image:: /_images/shop_release_showcase.png

**Evo** ist das neue responsive Standardtemplate für den JTL-Shop. Verantwortlich für die responsive Darstellung der Storefront ist das bekannte Framework **Bootstrap** in der Version 3.
Die Stylesheets (CSS-Dateien) werden, Dank des Evo-Editors, aus verschiedenen LESS-Dateien kompiliert. LESS bietet, im Gegensatz zu reinem CSS, viele Vorteile für Entwickler.
Mehr über die Vorteile der LESS-Technologie erfahren Sie unter http://lesscss.org.

Unter :doc:`/shop_templates/structure` finden Sie nähere Informationen zur Struktur des Evo-Templates.
