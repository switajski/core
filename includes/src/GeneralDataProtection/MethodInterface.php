<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\GeneralDataProtection;

/**
 * Interface MethodInterface
 * @package JTL\GeneralDataProtection
 */
interface MethodInterface
{
    /**
     *
     */
    public function execute(): void;
}
