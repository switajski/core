<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Boxes\Items;

/**
 * Class FilterItem
 *
 * @package JTL\Boxes\Items
 */
final class FilterItem extends AbstractBox
{

}
