<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace JTL\DB\Services;

/**
 * Interface GcServiceInterface
 * @package JTL\DB\Services
 */
interface GcServiceInterface
{
    /**
     * @return $this
     */
    public function run(): GcServiceInterface;
}
