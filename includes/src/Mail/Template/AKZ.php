<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Mail\Template;

/**
 * Class AKZ
 * @package JTL\Mail\Template
 */
class AKZ extends AbstractTemplate
{
    protected $id = \MAILTEMPLATE_AKZ;
}
