<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Mail\Template;

/**
 * Class Header
 * @package JTL\Mail\Template
 */
class Header extends AbstractTemplate
{
    protected $id = \MAILTEMPLATE_HEADER;
}
