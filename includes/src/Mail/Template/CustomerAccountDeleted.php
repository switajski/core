<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Mail\Template;

/**
 * Class CustomerAccountDeleted
 * @package JTL\Mail\Template
 */
class CustomerAccountDeleted extends AbstractTemplate
{
    protected $id = \MAILTEMPLATE_KUNDENACCOUNT_GELOESCHT;
}
