<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Mail\Template;

/**
 * Class NewAccountCreatedByAdmin
 * @package JTL\Mail\Template
 */
class NewAccountCreatedByAdmin extends AbstractTemplate
{
    protected $id = \MAILTEMPLATE_ACCOUNTERSTELLUNG_DURCH_BETREIBER;
}
