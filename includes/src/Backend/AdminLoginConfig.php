<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Backend;

/**
 * Class AdminLoginConfig
 * @package Backend
 */
abstract class AdminLoginConfig
{
    public const CONFIG_DB = 'db';

    public const CONFIG_FILE = 'file';
}
