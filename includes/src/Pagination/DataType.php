<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Pagination;

use MyCLabs\Enum\Enum;

/**
 * Class DataType
 * @package JTL\Pagination
 */
class DataType extends Enum
{
    public const TEXT = 0;

    public const NUMBER = 1;
}
