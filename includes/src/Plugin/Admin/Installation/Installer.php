<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Plugin\Admin\Installation;

use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\Text;
use JTL\Plugin\Admin\Validation\ValidatorInterface;
use JTL\Plugin\Helper;
use JTL\Plugin\InstallCode;
use JTL\Plugin\LegacyPluginLoader;
use JTL\Plugin\PluginInterface;
use JTL\Plugin\PluginLoader;
use JTL\Plugin\State;
use JTL\Shop;
use JTL\XMLParser;
use JTLShop\SemVer\Version;
use stdClass;
use function Functional\map;
use function Functional\select;

/**
 * Class Installer
 * @package JTL\Plugin\Admin\Installation
 */
final class Installer
{
    /**
     * @var DbInterface
     */
    private $db;

    /**
     * @var string
     */
    private $dir;

    /**
     * @var Uninstaller
     */
    private $uninstaller;

    /**
     * @var ValidatorInterface
     */
    private $pluginValidator;

    /**
     * @var ValidatorInterface
     */
    private $extensionValidator;

    /**
     * @var PluginInterface|null
     */
    private $plugin;

    /**
     * Installer constructor.
     * @param DbInterface        $db
     * @param Uninstaller        $uninstaller
     * @param ValidatorInterface $validator
     * @param ValidatorInterface $modernValidator
     */
    public function __construct(
        DbInterface $db,
        Uninstaller $uninstaller,
        ValidatorInterface $validator,
        ValidatorInterface $modernValidator
    ) {
        $this->db                 = $db;
        $this->uninstaller        = $uninstaller;
        $this->pluginValidator    = $validator;
        $this->extensionValidator = $modernValidator;
    }

    /**
     * @return string
     */
    public function getDir(): string
    {
        return $this->dir;
    }

    /**
     * @param string $dir
     */
    public function setDir(string $dir): void
    {
        $this->dir = $dir;
    }

    /**
     * @return PluginInterface|null
     */
    public function getPlugin(): ?PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @param PluginInterface|null $plugin
     */
    public function setPlugin(PluginInterface $plugin): void
    {
        $this->plugin = $plugin;
    }

    /**
     * @return int
     * @former installierePluginVorbereitung()
     */
    public function prepare(): int
    {
        if (empty($this->dir)) {
            return InstallCode::WRONG_PARAM;
        }
        $validator = $this->pluginValidator;
        $baseDir   = \PFAD_ROOT . \PFAD_PLUGIN . \basename($this->dir);
        if (!\file_exists($baseDir . '/' . \PLUGIN_INFO_FILE)) {
            $baseDir   = \PFAD_ROOT . \PLUGIN_DIR . \basename($this->dir);
            $validator = $this->extensionValidator;
            if (!\file_exists($baseDir . '/' . \PLUGIN_INFO_FILE)) {
                return InstallCode::INFO_XML_MISSING;
            }
        }
        $validator->setDir($baseDir);
        $parser = new XMLParser();
        $xml    = $parser->parse($baseDir . '/' . \PLUGIN_INFO_FILE);
        $code   = $validator->pluginPlausiIntern($xml, $this->plugin !== null);
        if ($code === InstallCode::DUPLICATE_PLUGIN_ID && $this->plugin !== null && $this->plugin->getID() > 0) {
            $code = InstallCode::OK;
        }
        if ($code === InstallCode::OK || $code === InstallCode::OK_LEGACY) {
            $code = $this->install($xml);
        }

        return $code;
    }

    /**
     * Installiert ein Plugin
     *
     * @param array $xml
     * @return int
     * @former installierePlugin()
     */
    public function install(array $xml): int
    {
        $baseNode           = $this->getBaseNode($xml);
        $versionNode        = $baseNode['Install'][0]['Version'] ?? null;
        $xmlVersion         = (int)$baseNode['XMLVersion'];
        $basePath           = \PFAD_ROOT . \PFAD_PLUGIN . $this->dir . \DIRECTORY_SEPARATOR;
        $lastVersionKey     = null;
        $plugin             = new stdClass();
        $plugin->nStatus    = $this->plugin === null ? State::ACTIVATED : $this->plugin->getState();
        $plugin->bExtension = 0;
        if (\is_array($versionNode)) {
            $lastVersionKey = \count($versionNode) / 2 - 1;
            $version        = (int)$versionNode[$lastVersionKey . ' attr']['nr'];
            $versionedDir   = $basePath . \PFAD_PLUGIN_VERSION . $version . \DIRECTORY_SEPARATOR;
            $bootstrapper   = $versionedDir . \OLD_BOOTSTRAPPER;
        } else {
            $version            = $baseNode['Version'];
            $basePath           = \PFAD_ROOT . \PLUGIN_DIR . $this->dir . \DIRECTORY_SEPARATOR;
            $versionedDir       = $basePath;
            $versionNode        = [];
            $bootstrapper       = $versionedDir . \PLUGIN_BOOTSTRAPPER;
            $plugin->bExtension = 1;
        }
        $plugin                       = $this->addLicenseData($baseNode, $plugin);
        $plugin->cName                = $baseNode['Name'];
        $plugin->cBeschreibung        = $baseNode['Description'];
        $plugin->cAutor               = $baseNode['Author'];
        $plugin->cURL                 = $baseNode['URL'];
        $plugin->cIcon                = $baseNode['Icon'] ?? null;
        $plugin->cVerzeichnis         = $this->dir;
        $plugin->cPluginID            = $baseNode['PluginID'];
        $plugin->cStoreID             = $baseNode['StoreID'] ?? null;
        $plugin->cFehler              = '';
        $plugin->nVersion             = $version;
        $plugin->nXMLVersion          = $xmlVersion;
        $plugin->nPrio                = 0;
        $plugin->dZuletztAktualisiert = 'NOW()';
        $plugin->dErstellt            = $lastVersionKey !== null
            ? $versionNode[$lastVersionKey]['CreateDate']
            : $baseNode['CreateDate'];
        $plugin->bBootstrap           = (int)\is_file($bootstrapper);
        $plugin                       = $this->checkLicense($versionedDir, $plugin);

        $plugin->dInstalliert = ($this->plugin !== null && $this->plugin->getID() > 0)
            ? $this->plugin->getMeta()->getDateInstalled()->format('Y-m-d H:i:s')
            : 'NOW()';
        $plugin->kPlugin      = $this->db->insert('tplugin', $plugin);
        $this->flushCache($baseNode);
        if ($plugin->kPlugin <= 0) {
            return InstallCode::WRONG_PARAM;
        }
        $factory = $plugin->bExtension === 0
            ? new LegacyPluginInstallerFactory($this->db, $xml, $plugin)
            : new PluginInstallerFactory($this->db, $xml, $plugin);
        $res     = $factory->install();
        if ($res !== InstallCode::OK) {
            $this->uninstaller->uninstall($plugin->kPlugin);

            return $res;
        }

        return $this->installSQL($plugin, $versionNode, $version, $versionedDir);
    }

    /**
     * @param stdClass $plugin
     * @param array    $versionNode
     * @param mixed    $version
     * @param string   $versionedDir
     * @return int
     */
    private function installSQL(stdClass $plugin, array $versionNode, $version, string $versionedDir): int
    {
        $loader      = $plugin->bExtension === 1
            ? new PluginLoader($this->db, Shop::Container()->getCache())
            : new LegacyPluginLoader($this->db, Shop::Container()->getCache());
        $hasSQLError = false;
        $code        = InstallCode::OK;
        foreach ($versionNode as $i => $versionData) {
            if ($version > 0
                && $this->plugin !== null
                && isset($versionData['nr'])
                && $this->plugin->getMeta()->getVersion() >= (int)$versionData['nr']
            ) {
                continue;
            }
            $i = (string)$i;
            \preg_match('/[0-9]+\sattr/', $i, $hits1);

            if (!isset($hits1[0]) || \mb_strlen($hits1[0]) !== \mb_strlen($i)) {
                continue;
            }
            $nVersionTMP = (int)$versionData['nr'];
            $xy          = \trim(\str_replace('attr', '', $i));
            $sqlFile     = $versionNode[$xy]['SQL'] ?? '';
            if ($sqlFile === '') {
                continue;
            }
            $code = $this->validateSQL($sqlFile, $nVersionTMP, $plugin);
            if ($code !== InstallCode::OK) {
                $hasSQLError = true;
                break;
            }
        }
        if ($plugin->bExtension === 1) {
            $this->updateByMigration($plugin, $versionedDir, Version::parse($version));
        }
        // Ist ein SQL Fehler aufgetreten? Wenn ja, deinstalliere wieder alles
        if ($hasSQLError) {
            $this->uninstaller->uninstall($plugin->kPlugin);
        }
        if ($code === InstallCode::OK
            && $this->plugin === null
            && ($p = Helper::bootstrap($plugin->kPlugin, $loader)) !== null
        ) {
            $p->installed();
        }
        if ($this->plugin !== null && ($code === InstallCode::OK_LEGACY || $code === InstallCode::OK)) {
            $code = $this->syncPluginUpdate($plugin->kPlugin);
            if (($p = Helper::bootstrap($this->plugin->getID(), $loader)) !== null) {
                $p->updated($this->plugin->getMeta()->getVersion(), $version);
            }
        }

        return $code;
    }

    /**
     * @param array    $baseNode
     * @param stdClass $plugin
     * @return stdClass
     */
    private function addLicenseData(array $baseNode, stdClass $plugin): stdClass
    {
        $plugin->cLizenz           = '';
        $plugin->cLizenzKlasse     = '';
        $plugin->cLizenzKlasseName = '';
        if (isset($baseNode['LicenceClass'], $baseNode['LicenceClassFile'])
            && \mb_strlen($baseNode['LicenceClass']) > 0
            && \mb_strlen($baseNode['LicenceClassFile']) > 0
        ) {
            $plugin->cLizenzKlasse     = $baseNode['LicenceClass'];
            $plugin->cLizenzKlasseName = $baseNode['LicenceClassFile'];
            $plugin->nStatus           = State::LICENSE_KEY_MISSING;
        }

        return $plugin;
    }

    /**
     * @param array $baseNode
     */
    private function flushCache(array $baseNode): void
    {
        $tags        = empty($baseNode['Install'][0]['FlushTags'])
            ? []
            : \explode(',', $baseNode['Install'][0]['FlushTags']);
        $tagsToFlush = map(select($tags, function ($e) {
            return \defined(\trim($e));
        }), function ($e) {
            return \constant(\trim($e));
        });
        if (\count($tagsToFlush) > 0) {
            Shop::Container()->getCache()->flushTags($tagsToFlush);
        }
    }

    /**
     * @param string   $versionedDir
     * @param stdClass $plugin
     * @return stdClass
     */
    private function checkLicense(string $versionedDir, stdClass $plugin): stdClass
    {
        $licenceClassFile = $versionedDir . \PFAD_PLUGIN_LICENCE . $plugin->cLizenzKlasseName;
        if ($this->plugin !== null
            && \is_file($licenceClassFile)
            && $this->plugin->getState() > 0
            && $this->plugin->getLicense()->hasLicense()
        ) {
            require_once $licenceClassFile;
            $pluginLicence = new $plugin->cLizenzKlasse();
            $licenceMethod = \PLUGIN_LICENCE_METHODE;
            if ($pluginLicence->$licenceMethod($this->plugin->getLicense()->getKey())) {
                $plugin->cLizenz = $this->plugin->getLicense()->getKey();
                $plugin->nStatus = $this->plugin->getState();
            }
        }

        return $plugin;
    }

    /**
     * @param array $xml
     * @return array
     */
    private function getBaseNode(array $xml): array
    {
        return $xml['jtlshopplugin'][0] ?? $xml['jtlshop3plugin'][0];
    }

    /**
     * Geht die angegebene SQL durch und formatiert diese. Immer 1 SQL pro Zeile.
     *
     * @param string $sqlFile
     * @param string $pluginName
     * @param int    $pluginVersion
     * @return array
     */
    private function parseSQLFile(string $sqlFile, string $pluginName, $pluginVersion): array
    {
        $file = \PFAD_ROOT . \PFAD_PLUGIN . $pluginName . '/' .
            \PFAD_PLUGIN_VERSION . $pluginVersion . '/' .
            \PFAD_PLUGIN_SQL . $sqlFile;

        if (!\file_exists($file)) {
            return [];// SQL Datei existiert nicht
        }
        $handle   = \fopen($file, 'r');
        $sqlLines = [];
        $line     = '';
        while (($data = \fgets($handle)) !== false) {
            $data = \trim($data);
            if ($data !== '' && \mb_strpos($data, '--') !== 0) {
                if (\mb_strpos($data, 'CREATE TABLE') !== false) {
                    $line .= \trim($data);
                } elseif (\mb_strpos($data, 'INSERT') !== false) {
                    $line .= \trim($data);
                } else {
                    $line .= \trim($data);
                }

                if (\mb_substr($data, \mb_strlen($data) - 1, 1) === ';') {
                    $sqlLines[] = $line;
                    $line       = '';
                }
            }
        }
        \fclose($handle);

        return $sqlLines;
    }

    /**
     * @param stdClass $plugin
     * @param string    $pluginPath
     * @param Version   $targetVersion
     * @return array|Version
     * @throws \Exception
     */
    private function updateByMigration(stdClass $plugin, string $pluginPath, Version $targetVersion)
    {
        $path              = $pluginPath . \DIRECTORY_SEPARATOR . \PFAD_PLUGIN_MIGRATIONS;
        $manager           = new MigrationManager($this->db, $path, $plugin->cPluginID, $targetVersion);
        $pendingMigrations = $manager->getPendingMigrations();
        if (\count($pendingMigrations) === 0) {
            return $targetVersion;
        }

        return $manager->migrate();
    }

    /**
     * @param string    $sqlFile
     * @param int       $version
     * @param stdClass $plugin
     * @return int
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     * @former logikSQLDatei()
     */
    private function validateSQL(string $sqlFile, $version, stdClass $plugin): int
    {
        if (empty($sqlFile)
            || (int)$version < 100
            || (int)$plugin->kPlugin <= 0
            || empty($plugin->cPluginID)
        ) {
            return InstallCode::SQL_MISSING_DATA;
        }
        $lines = $this->parseSQLFile($sqlFile, $plugin->cVerzeichnis, $version);
        if (\count($lines) === 0) {
            return InstallCode::SQL_INVALID_FILE_CONTENT;
        }
        $sqlRegEx = '/xplugin[_]{1}' . $plugin->cPluginID . '[_]{1}[a-zA-Z0-9_]+/';
        foreach ($lines as $sql) {
            $sql = Text::removeNumerousWhitespaces($sql);
            if (\mb_stripos($sql, 'create table') !== false) {
                // when using "create table if not exists" statement, the table name is at index 5, otherwise at 2
                $index = (\mb_stripos($sql, 'create table if not exists') !== false) ? 5 : 2;
                $tmp   = \explode(' ', $sql);
                $table = \str_replace(["'", '`'], '', $tmp[$index]);
                \preg_match($sqlRegEx, $table, $hits);
                if (!isset($hits[0]) || \mb_strlen($hits[0]) !== \mb_strlen($table)) {
                    return InstallCode::SQL_WRONG_TABLE_NAME_CREATE;
                }
                $exists = $this->db->select('tplugincustomtabelle', 'cTabelle', $table);
                if (!isset($exists->kPluginCustomTabelle) || !$exists->kPluginCustomTabelle) {
                    $customTable           = new stdClass();
                    $customTable->kPlugin  = $plugin->kPlugin;
                    $customTable->cTabelle = $table;

                    $this->db->insert('tplugincustomtabelle', $customTable);
                }
            } elseif (\mb_stripos($sql, 'drop table') !== false) {
                // SQL versucht eine Tabelle zu löschen => prüfen ob es sich um eine Plugintabelle handelt
                // when using "drop table if exists" statement, the table name is at index 5, otherwise at 2
                $index = (\mb_stripos($sql, 'drop table if exists') !== false) ? 4 : 2;
                $tmp   = \explode(' ', Text::removeNumerousWhitespaces($sql));
                $table = \str_replace(["'", '`'], '', $tmp[$index]);
                \preg_match($sqlRegEx, $table, $hits);
                if (\mb_strlen($hits[0]) !== \mb_strlen($table)) {
                    return InstallCode::SQL_WRONG_TABLE_NAME_DELETE;
                }
            }

            $this->db->query($sql, ReturnType::DEFAULT);
            $errNo = $this->db->getErrorCode();
            if ($errNo) {
                Shop::Container()->getLogService()->withName('kPlugin')->error(
                    'SQL Fehler beim Installieren des Plugins (' . $plugin->cName . '): ' .
                    \str_replace("'", '', $this->db->getErrorMessage()),
                    [$plugin->kPlugin]
                );

                return InstallCode::SQL_ERROR;
            }
        }

        return InstallCode::OK;
    }

    /**
     * Wenn ein Update erfolgreich mit neuer kPlugin in der Datenbank ist
     * wird der alte kPlugin auf die neue Version übertragen und
     * die alte Plugin-Version deinstalliert.
     *
     * @param int $pluginID
     * @return int
     * 1 = Alles O.K.
     * 2 = Übergabeparameter nicht korrekt
     * 3 = Update konnte nicht installiert werden
     */
    public function syncPluginUpdate(int $pluginID): int
    {
        $oldPluginID = $this->plugin->getID();
        $res         = $this->uninstaller->uninstall($oldPluginID, true, $pluginID);
        if ($res !== InstallCode::OK) {
            $this->uninstaller->uninstall($pluginID);

            return InstallCode::SQL_ERROR;
        }
        $upd = (object)['kPlugin' => $oldPluginID];
        $this->db->update('tplugin', 'kPlugin', $pluginID, $upd);
        $this->db->update('tpluginhook', 'kPlugin', $pluginID, $upd);
        $this->db->update('tpluginadminmenu', 'kPlugin', $pluginID, $upd);
        $this->db->update('tpluginsprachvariable', 'kPlugin', $pluginID, $upd);
        $this->db->update('tadminwidgets', 'kPlugin', $pluginID, $upd);
        $this->db->update('tplugin_resources', 'kPlugin', $pluginID, $upd);
        $this->db->update('tplugincustomtabelle', 'kPlugin', $pluginID, $upd);
        $this->db->update('tplugintemplate', 'kPlugin', $pluginID, $upd);
        $this->db->update('tpluginlinkdatei', 'kPlugin', $pluginID, $upd);
        $this->db->update('texportformat', 'kPlugin', $pluginID, $upd);
        $this->db->update('topcportlet', 'kPlugin', $pluginID, $upd);
        $this->db->update('topcblueprint', 'kPlugin', $pluginID, $upd);

        $this->updateLangVars($oldPluginID, $pluginID);
        $this->updateConfig($oldPluginID, $pluginID);
        $this->db->update(
            'tboxvorlage',
            ['kCustomID', 'eTyp'],
            [$pluginID, 'plugin'],
            (object)['kCustomID' => $oldPluginID]
        );
        $this->updateMailTemplates($oldPluginID, $pluginID);
        $this->db->update('tlink', 'kPlugin', $pluginID, (object)['kPlugin' => $oldPluginID]);
        // tboxen
        // Ausnahme: Gibt es noch eine Boxenvorlage in der Pluginversion?
        // Falls nein -> lösche tboxen mit dem entsprechenden kPlugin
        $data = $this->db->select('tboxvorlage', 'kCustomID', $oldPluginID, 'eTyp', 'plugin');
        if (isset($data->kBoxvorlage) && (int)$data->kBoxvorlage > 0) {
            $upd              = new stdClass();
            $upd->kBoxvorlage = $data->kBoxvorlage;
            $this->db->update('tboxen', 'kCustomID', $oldPluginID, $upd);
        } else {
            $this->db->delete('tboxen', 'kCustomID', $oldPluginID);
        }
        $upd = (object)['kPlugin' => $oldPluginID];
        $this->db->update('tcheckboxfunktion', 'kPlugin', $pluginID, $upd);
        $this->db->update('tspezialseite', 'kPlugin', $pluginID, $upd);
        $this->updatePaymentMethods($oldPluginID, $pluginID);

        return InstallCode::OK;
    }

    /**
     * @param int $oldPluginID
     * @param int $pluginID
     */
    private function updateLangVars(int $oldPluginID, int $pluginID): void
    {
        $this->db->update(
            'tpluginsprachvariablecustomsprache',
            'kPlugin',
            $pluginID,
            (object)['kPlugin' => $oldPluginID]
        );
        $customLangVars = $this->db->queryPrepared(
            'SELECT DISTINCT tpluginsprachvariable.kPluginSprachvariable AS newID,
                tpluginsprachvariablecustomsprache.kPluginSprachvariable AS oldID, tpluginsprachvariable.cName
                FROM tpluginsprachvariablecustomsprache
                JOIN tpluginsprachvariable
                    ON tpluginsprachvariable.cName =  tpluginsprachvariablecustomsprache.cSprachvariable
                WHERE tpluginsprachvariablecustomsprache.kPlugin = :pid',
            ['pid' => $oldPluginID],
            ReturnType::ARRAY_OF_OBJECTS,
            true
        );
        foreach ($customLangVars as $langVar) {
            $this->db->update(
                'tpluginsprachvariablecustomsprache',
                ['kPlugin', 'kPluginSprachvariable'],
                [$oldPluginID, $langVar->oldID],
                (object)['kPluginSprachvariable' => $langVar->newID]
            );
        }
    }

    /**
     * @param int $oldPluginID
     * @param int $pluginID
     */
    private function updateConfig(int $oldPluginID, int $pluginID): void
    {
        $pluginConf = $this->db->query(
            'SELECT *
                FROM tplugineinstellungen
                WHERE kPlugin IN (' . $oldPluginID . ', ' . $pluginID . ')
                ORDER BY kPlugin',
            ReturnType::ARRAY_OF_OBJECTS
        );
        if (\count($pluginConf) > 0) {
            $confData = [];
            foreach ($pluginConf as $conf) {
                $name = \str_replace(
                    ['kPlugin_' . $oldPluginID . '_', 'kPlugin_' . $pluginID . '_'],
                    '',
                    $conf->cName
                );
                if (!isset($confData[$name])) {
                    $confData[$name]          = new stdClass();
                    $confData[$name]->kPlugin = $oldPluginID;
                    $confData[$name]->cName   = \str_replace(
                        'kPlugin_' . $pluginID . '_',
                        'kPlugin_' . $oldPluginID . '_',
                        $conf->cName
                    );
                    $confData[$name]->cWert   = $conf->cWert;
                }
            }
            $this->db->query(
                'DELETE FROM tplugineinstellungen
                    WHERE kPlugin IN (' . $oldPluginID . ', ' . $pluginID . ')',
                ReturnType::AFFECTED_ROWS
            );

            foreach ($confData as $value) {
                $this->db->insert('tplugineinstellungen', $value);
            }
        }
        $this->db->query(
            'UPDATE tplugineinstellungen
                SET kPlugin = ' . $oldPluginID . ",
                    cName = REPLACE(cName, 'kPlugin_" . $pluginID . "_', 'kPlugin_" . $oldPluginID . "_')
                WHERE kPlugin = " . $pluginID,
            ReturnType::AFFECTED_ROWS
        );
        $this->db->query(
            'UPDATE tplugineinstellungenconf
                SET kPlugin = ' . $oldPluginID . ",
                    cWertName = REPLACE(cWertName, 'kPlugin_" . $pluginID . "_', 'kPlugin_" . $oldPluginID . "_')
                WHERE kPlugin = " . $pluginID,
            ReturnType::AFFECTED_ROWS
        );
    }

    /**
     * @param int $oldPluginID
     * @param int $pluginID
     */
    private function updateMailTemplates(int $oldPluginID, int $pluginID): void
    {
        $this->db->update('temailvorlage', 'kPlugin', $pluginID, (object)['kPlugin' => $oldPluginID]);
        $oldMailTpl = $this->db->select('temailvorlage', 'kPlugin', $oldPluginID);
        $newMailTpl = $this->db->select('temailvorlage', 'kPlugin', $pluginID);
        $newTplID   = 0;
        $oldTplID   = 0;
        if (isset($newMailTpl->kEmailvorlage, $oldMailTpl->kEmailvorlage)) {
            $this->db->update(
                'tpluginemailvorlageeinstellungen',
                'kEmailvorlage',
                $oldMailTpl->kEmailvorlage,
                (object)['kEmailvorlage' => $newMailTpl->kEmailvorlage]
            );
        }
        foreach ($this->plugin->getMailTemplates()->getTemplatesAssoc() as $moduleID => $oldTpl) {
            $newTpl = $this->db->select(
                'temailvorlage',
                'kPlugin',
                $oldPluginID,
                'cModulId',
                $moduleID,
                null,
                null,
                false,
                'kEmailvorlage'
            );
            if (isset($newTpl->kEmailvorlage) && $newTpl->kEmailvorlage > 0) {
                if ($newTplID === 0 || $oldTplID === 0) {
                    $newTplID = (int)$newTpl->kEmailvorlage;
                    $oldTplID = (int)$oldTpl->kEmailvorlage;
                }
                $this->db->update(
                    'temailvorlagesprache',
                    'kEmailvorlage',
                    $oldTpl->kEmailvorlage,
                    (object)['kEmailvorlage' => $newTpl->kEmailvorlage]
                );
            }
        }
        $this->db->update(
            'tpluginemailvorlageeinstellungen',
            'kEmailvorlage',
            $oldTplID,
            (object)['kEmailvorlage' => $newTplID]
        );
    }

    /**
     * @param int $oldPluginID
     * @param int $pluginID
     */
    private function updatePaymentMethods(int $oldPluginID, int $pluginID): void
    {
        $this->db->query(
            'UPDATE tpluginzahlungsartklasse
                SET kPlugin = ' . $oldPluginID . ",
                    cModulId = REPLACE(cModulId, 'kPlugin_" . $pluginID . "_', 'kPlugin_" . $oldPluginID . "_')
                WHERE kPlugin = " . $pluginID,
            ReturnType::AFFECTED_ROWS
        );
        $oldPaymentMethods = $this->db->queryPrepared(
            'SELECT kZahlungsart, cModulId
                FROM tzahlungsart
                WHERE cModulId LIKE :newID',
            ['newID' => 'kPlugin_' . $oldPluginID . '_%'],
            ReturnType::ARRAY_OF_OBJECTS
        );
        foreach ($oldPaymentMethods as $method) {
            $oldModuleID      = \str_replace(
                'kPlugin_' . $oldPluginID . '_',
                'kPlugin_' . $pluginID . '_',
                $method->cModulId
            );
            $newPaymentMethod = $this->db->queryPrepared(
                'SELECT kZahlungsart
                    FROM tzahlungsart
                    WHERE cModulId LIKE :oldID',
                ['oldID' => $oldModuleID],
                ReturnType::SINGLE_OBJECT
            );
            $setSQL           = '';
            if (isset($method->kZahlungsart, $newPaymentMethod->kZahlungsart)) {
                $this->db->query(
                    'DELETE tzahlungsart, tzahlungsartsprache
                        FROM tzahlungsart
                        JOIN tzahlungsartsprache
                            ON tzahlungsartsprache.kZahlungsart = tzahlungsart.kZahlungsart
                        WHERE tzahlungsart.kZahlungsart = ' . $method->kZahlungsart,
                    ReturnType::AFFECTED_ROWS
                );

                $setSQL = ' , kZahlungsart = ' . $method->kZahlungsart;
                $upd    = (object)['kZahlungsart' => $method->kZahlungsart];
                $this->db->update('tzahlungsartsprache', 'kZahlungsart', $newPaymentMethod->kZahlungsart, $upd);
            }

            $this->db->queryPrepared(
                'UPDATE tzahlungsart
                    SET cModulId = :newID ' . $setSQL . '
                    WHERE cModulId LIKE :oldID',
                ['oldID' => $oldModuleID, 'newID' => $method->cModulId],
                ReturnType::AFFECTED_ROWS
            );
        }
    }
}
