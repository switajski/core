<?php declare(strict_types=1);
/**
 * @copyright (c) JTL-Software-GmbH
 * @license       http://jtl-url.de/jtlshoplicense
 */

namespace JTL\Plugin\Admin\Installation;

/**
 * Class LegacyPluginInstallerFactory
 * @package JTL\Plugin\Admin\Installation
 */
class LegacyPluginInstallerFactory extends AbstractInstallerFactory
{
}
