<?php
/**
 * @copyright (c) JTL-Software-GmbH
 * @license http://jtl-url.de/jtlshoplicense
 * @deprecated since 5.0.0 - use JTL\Plugin\Payment\Method instead
 */

class_alias(\JTL\Plugin\Payment\LegacyMethod::class, 'PaymentMethod', true);
