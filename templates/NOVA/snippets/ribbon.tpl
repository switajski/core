{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='snippets-ribbon'}
    {if !empty($Artikel->Preise->Sonderpreis_aktiv)}
        {$sale = $Artikel->Preise->discountPercentage}
    {/if}

    <div class="ribbon-{$Einstellungen.template.productlist.ribbon_type}
        ribbon-{$Einstellungen.template.productlist.ribbon_position}
        ribbon--{$Artikel->oSuchspecialBild->getType()}">
        {block name='snippets-ribbon-content'}
            <span>
                {lang key='ribbon-'|cat:$Artikel->oSuchspecialBild->getType() section='productOverview' printf=$sale|default:''|cat:'%'}
            </span>
        {/block}
    </div>
{/block}