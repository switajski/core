{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='register-form-customer-login'}
    {block name='register-form-customer-login-email'}
        {formgroup label="{lang key='email' section='account data'}" label-for="login_email"}
            {input type="text"
                name="email"
                id="login_email"
                placeholder="{lang key='email' section='account data'}"
                required=true
                autocomplete="email"
                value=""
            }
        {/formgroup}
    {/block}
    {block name='register-form-customer-login-password'}
        {formgroup label="{lang key='password' section='account data'}" label-for="login_password"}
            {input type="password"
                name="passwort"
                id="login_password"
                placeholder="{lang key='password' section='account data'}"
                required=true
                autocomplete="current-password"
                value=""
            }
        {/formgroup}
    {/block}
    {if isset($showLoginCaptcha) && $showLoginCaptcha}
        {block name='register-form-customer-login-captcha'}
            <div class="form-group text-center">
                {captchaMarkup getBody=true}
            </div>
        {/block}
    {/if}

    {block name='register-form-customer-login-submit'}
        {formgroup}
            {input type="hidden" name="login" value="1"}
            {input type="hidden" name="wk" value="{if isset($one_step_wk)}{$one_step_wk}{else}0{/if}"}
            {if !empty($oRedirect->cURL)}
                {foreach $oRedirect->oParameter_arr as $oParameter}
                    {input type="hidden" name=$oParameter->Name value=$oParameter->Wert}
                {/foreach}
                {input type="hidden" name="r" value=$oRedirect->nRedirect}
                {input type="hidden" name="cURL" value=$oRedirect->cURL}
            {/if}
            {row}
                {col cols=12 lg=6}
                    {button type="submit" variant="primary" block=true}
                        {lang key='login' section='checkout'}
                    {/button}
                {/col}
                {col cols=12 lg=6}
                    {link class="btn btn-link btn-block text-decoration-underline" href="{get_static_route id='pass.php'}"}
                        {lang key='forgotPassword'}
                    {/link}
                {/col}
            {/row}
        {/formgroup}
    {/block}
{/block}
