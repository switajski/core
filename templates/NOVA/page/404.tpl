{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='page-404'}
    <div id="page-not-found">
        {block name='page-404-include-sitemap'}
            {include file='page/sitemap.tpl'}
        {/block}
    </div>
{/block}
