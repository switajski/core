{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
{block name='boxes-box-container'}
    <div class="box box-container" id="sidebox{$oBox->getID()}">
        {$oBox->getHTML()}
    </div>
{/block}
