{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
<div id="page-not-found">
    {include file='page/sitemap.tpl'}
</div>
