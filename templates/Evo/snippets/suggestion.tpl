{**
 * @copyright (c) JTL-Software-GmbH
 * @license https://jtl-url.de/jtlshoplicense
 *}
<div>{$result->keyword} <span class="badge pull-right">{$result->quantity}</span></div>
