��    W      �     �      �  
   �     �     �     �     �     �     �          &     5     J     W  
   h     s  
   �     �  
   �     �     �     �     �     �     �  	   	     	     	     $	     1	     I	     Q	  	   a	     k	     w	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     
     "
     8
     G
     V
     e
     r
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     
               %     3     ?  
   L     W     f     t     �     �     �     �     �     �     �          "     5     J     \     k     ~     �  (   �     �      �               8     N     `     y     �     �     �     �     �     �  
   �     �  
                  2  #   R  	   v     �     �  	   �     �  	   �     �     �     �     �  	   �     �     
     $     -     9     B     G     L  �  R     �     �  �     \   �     	     %     >     N     a     w  W   �     �     �       
        '     /     =  0   O  1   �     �     �     �     �     �     �     �               (     @     _     x     �     �     �  	   �  	   �     �     �     �     �       w   1  )   �     *   -          4   /      (   N                3   B       $              M       <           G              ?   0          8       ;       	                 K                     Q   &          E                             F          @   T   U   J   )           :   .   %       =   D   S   9       A      !   5   V   ,   7       2                 6   +      O          P   >          L              W      1   H   R         I   '   "             
   #   C                    Bestseller Eigene Box (Mit Rahmen) Eigene Box (Ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Suchspecial) Filter (Tag) Globale Merkmale Hersteller In Kürze Verfügbar Kategorien Konfigurator Linkgruppe Login Neue im Sortiment News (Monatsübersicht) News Kategorien Schnellkauf Sonderangebot Suchwolke Tagwolke Top Angebot Top bewertet Trusted Shops - Reviews Umfrage Vergleichsliste Warenkorb Wunschliste Zuletzt angesehen activateOnAnyPage boxEdit boxLabel boxTemplate boxTitle boxType boxen boxenDesc boxenURL catBoxNum catBoxNumTooltip confirmDeleteBox deactivateOnAnyPage deactivatedOnAllPages deleteSelected errorBoxCreate errorBoxDelete errorBoxEdit errorBoxesVisibilityEdit errorContainerCreate inContainer invisibleBoxes linkgroup new newContainer noBoxActivated noBoxesAvailableFor noTemplateConfig pleaseSelect remove resort sectionBottom sectionLeft sectionRight sectionTop showBoxOnlyFor showContainer successBoxCreate successBoxDelete successBoxEdit successBoxRefresh successContainerCreate templateTypeCategory templateTypeContent templateTypeExtension templateTypeLinkList templateTypePlugin templateTypeTemplate visibleOnAllPages visibleOnPages visibleOnSomePages warningChangesForAllPages warningInvisibleBoxes Content-Type: text/plain; charset=UTF-8
 Bestsellers Custom shipping box (with frame) Custom box (without frame) Filter (customer review) Filter (manufacturer) Filter (category) Filter (characteristics) Filter (price range) Filter (search) Filter (item overlay) Filter (tag) Global characteristics Manufacturer Available soon Categories Configurator Link group Login New in product range News: months with news postings News: categories of the news system Quick buy Special offer Search cloud Tag cloud Top item Top rated Trusted Shops – Reviews Survey Comparison list Basket Wish list Recently viewed Activate %s on every page Edit box Description Template Name Type Boxes Here you can add boxes to different box containers or remove them. You can specify for each container on which pages it should be visible. Whether a specific container is available or not is determined by the template. You can create customised boxes, e.g. link groups or boxes with an individual xHTML content, in a box container. Some boxes can also be further configured via the Edit button. https://jtl-url.de/li846 Category box ID Only lists categories with the category attribute "category box" and the set number. For all categories with or without a function attribute, the default value is 0. Delete box "%s"? If you delete a container, all boxes in the container will also be deleted. Deactivate %s on every page deactivated on all pages Delete selected Could not add box. Could not remove box. Could not edit box. Could not set box visibility due to a MySQL error. Please contact the JTL Support team. Could not create container. In container Invisible boxes Link group New box New container No box activated. Currently there are no boxes available for "%s". Could not find configuration file (template.xml). Please select Remove from list Sort Footer Left side bar Right side bar Header Show box "%s" only for: Show container Box added successfully.  Box(es) deleted successfully. Box successfully edited. The boxes were updated. Container created successfully. Category Contents Extension Link list Plug-in Template visible on all pages Visible on the following pages visible on some pages The selection "All pages" overwrites existing changes to the boxes that were performed for individual pages beforehand. Some boxes cannot currently be displayed. 