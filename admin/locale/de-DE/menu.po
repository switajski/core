msgid ""
msgstr ""
"Project-Id-Version: Internationalisierung\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: fg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"X-Generator: Poedit 2.0.4\n"
"X-Poedit-SourceCharset: UTF-8\n"

# Titles
msgid "JTL Shop Administration"
msgstr "Administration von JTL-Shop"

msgid "Toggle navigation"
msgstr "Navigation umschalten"

msgid "Search item"
msgstr "Suchbegriff"

# Top Menu
msgid "Presentation"
msgstr "Darstellung"

# Mid Menu
msgid "Template"
msgstr "Templates"

# Menu Item
msgid "Template settings"
msgstr "Template-Einstellungen"

# Mid Menu
msgid "Images"
msgstr "Bilder"

# Menu Items
msgid "Image settings"
msgstr "Bildeinstellungen"

msgid "Watermark"
msgstr "Wasserzeichen"

msgid "Shop logo"
msgstr "Shop-Logo"

msgid "Item overlays"
msgstr "Artikel-Overlays"

# Top Menu
msgid "Contents"
msgstr "Inhalte"

# Mid Menu
msgid "Item"
msgstr "Artikel"

# Menu Items
msgid "Selection wizard"
msgstr "Auswahlassistent"

msgid "Item details"
msgstr "Artikeldetails"

msgid "Customer reviews"
msgstr "Kundenbewertungen"

msgid "Price history"
msgstr "Preisverlauf"

msgid "Warehouse"
msgstr "Warenlager"

# Top Menu
msgid "Pages"
msgstr "Seiten"

# Mid Menu
msgid "OnPage Composer"
msgstr "OnPage Composer"

# Menu Items
msgid "Front page"
msgstr "Startseite"

msgid "GTC/cancellation policy"
msgstr "AGB / Widerrufsbelehrung"

msgid "Own pages"
msgstr "Eigene Seiten"

msgid "News"
msgstr "News"

msgid "Box settings"
msgstr "Boxeneinstellungen"

msgid "Boxes"
msgstr "Boxen"

msgid "Language administration"
msgstr "Sprachverwaltung"

# Just incase you want to add a translation for partners:
#   msgid "IT-Recht Kanzlei"
#   msgstr "IT-Recht Kanzlei"
#
#   msgid "Trustbadge Reviews"
#   msgstr "Trustbadge Reviews"
# Mid Menu
msgid "Forms"
msgstr "Formulare"

# Menu Items
msgid "Form settings"
msgstr "Formulareinstellungen"

msgid "Contact form"
msgstr "Kontaktformular"

msgid "Custom form fields"
msgstr "Eigene Formularfelder"

msgid "Check box administration"
msgstr "Checkboxenverwaltung"

# Mid Menu
msgid "E-mails"
msgstr "E-Mails"

# Menu Items
msgid "Email settings"
msgstr "E-Mail-Einstellungen"

msgid "Templates"
msgstr "Vorlagen"

msgid "Reports"
msgstr "Berichte"

msgid "Blacklist"
msgstr "Blacklist"

msgid "E-mail log"
msgstr "E-Mail-Log"

# Top Menu
msgid "Purchases"
msgstr "Kaufabwicklung"

# Mid Menu
msgid "Shopping cart"
msgstr "Warenkorb"

# Menu Items
msgid "Shopping cart settings"
msgstr "Warenkorbeinstellungen"

msgid "Saved baskets"
msgstr "Gespeicherte Warenkörbe"

msgid "Free gift"
msgstr "Gratisgeschenk"

# Just incase you want to add a translation for partners:
#   msgid "Trusted Shops"
#   msgstr "Trusted Shops"
# Mid Menu
msgid "Payments"
msgstr "Zahlungen"

# Menu Items
msgid "Method of payment"
msgstr "Zahlungsarten"

# Just incase you want to add a translation for partners:
#   msgid "Amazon Payments"
#   msgstr "Amazon Pay"
#
#   msgid "Skrill"
#   msgstr "Skrill"
# Mid Menu
msgid "Delivery"
msgstr "Versand"

# Menu Items
msgid "Shipping methods"
msgstr "Versandarten"

msgid "Additional packaging"
msgstr "Zusatzverpackung"

msgid "Marketing"
msgstr "Marketing"

msgid "Customer loyalty"
msgstr "Kundenbindung"

msgid "Surveys"
msgstr "Umfragen"

msgid "Customers win customers"
msgstr "Kunden werben Kunden"

msgid "Coupons"
msgstr "Coupons"

msgid "Wish list"
msgstr "Wunschzettel"

msgid "Comparison list"
msgstr "Vergleichsliste"

msgid "Newsletter"
msgstr "Newsletter"

# Mid Menu
msgid "Publicity"
msgstr "Werbung"

# Menu Items
msgid "Banner"
msgstr "Banner"

msgid "Slider"
msgstr "Slider"

msgid "Campaigns"
msgstr "Kampagnen"

# Mid Menu
msgid "Statistics"
msgstr "Statistiken"

# Menu Items
msgid "Sales revenues"
msgstr "Umsätze"

msgid "Visitors"
msgstr "Besucher"

msgid "Customer origin"
msgstr "Kundenherkunft"

msgid "Search engines"
msgstr "Suchmaschinen"

msgid "Start pages"
msgstr "Einstiegsseiten"

msgid "Coupon statistics"
msgstr "Coupon-Statistiken"

# Top Menu
msgid "Import / Export"
msgstr "Import / Export"

# Mid Menu
msgid "Import"
msgstr "Import"

# Menu Items
msgid "Customer data import"
msgstr "Kundenimport"

msgid "Import newsletter recipient"
msgstr "Newsletter-Empfänger importieren"

msgid "Zip code import"
msgstr "PLZ-Import"

# Mid Menu
msgid "Export"
msgstr "Export"

# Menu Items
msgid "Task scheduler"
msgstr "Aufgabenplaner"

msgid "Export formats"
msgstr "Exportformate"

msgid "Sitemap export"
msgstr "Sitemap-Export"

msgid "RSS feed"
msgstr "RSS-Feed"

# Top Menu
msgid "Administration"
msgstr "Verwaltung"

# Mid Menu
msgid "Settings"
msgstr "Einstellungen"

# Menu Items
msgid "Global settings"
msgstr "Globale Einstellungen"

msgid "Back end user"
msgstr "Backend-Benutzer"

msgid "Synchronisation with JTL-Wawi"
msgstr "Synchronisierung mit JTL-Wawi"

msgid "Global meta data"
msgstr "Globale Metadaten"

msgid "Special items"
msgstr "Besondere Artikel"

msgid "Sitemap structure"
msgstr "Sitemap-Aufbau"

msgid "Number formats"
msgstr "Zahlenformate"

msgid "Cache"
msgstr "Cache"

# Mid Menu
msgid "Maintenance"
msgstr "Wartung"

# Menu Items
msgid "Order history"
msgstr "Bestellhistorie"

msgid "Activation centre"
msgstr "Freischaltzentrale"

msgid "System log"
msgstr "System-Log"

msgid "Status"
msgstr "Status"

msgid "Update"
msgstr "Update"

msgid "Image administration"
msgstr "Bilderverwaltung"

msgid "Re-directions"
msgstr "Weiterleitungen"

msgid "Reset shop"
msgstr "Shop zurücksetzen"

# Mid Menu
msgid "Search"
msgstr "Suche"

# Menu Items
msgid "Search settings"
msgstr "Sucheinstellungen"

msgid "Filter"
msgstr "Filter"

msgid "Queries"
msgstr "Suchanfragen"

msgid "Cron"
msgstr "Cron"

# Top Menu
msgid "Plugins"
msgstr "Plugins"

# Mid Menu
msgid "Overview"
msgstr "Übersicht"

# Menu Items
msgid "Plug-in marketplace"
msgstr "Plugin-Marktplatz"

msgid "Plug-in administration"
msgstr "Plugin-Verwaltung"

msgid "Plug-in profiler"
msgstr "Plugin-Profiler"

# Mid Menu
# This is redundant, but could be used in the future.
#   msgid "Plugins"
#   msgstr "Plugins"
#
#
# Right Top Menu
msgid "Messages"
msgstr "Mitteilungen"

msgid "Favorites"
msgstr "Favoriten"

msgid "Dashboard"
msgstr "Dashboard"

msgid "Help"
msgstr "Hilfe"

msgid "System check"
msgstr "System-Check"

msgid "Rating system"
msgstr "Bewertungssystem"

msgid "Manage favorites"
msgstr "Favoriten verwalten"

msgid "First steps"
msgstr "Erste Schritte"

msgid "JTL guide"
msgstr "JTL-Guide"

msgid "JTL forum"
msgstr "JTL-Forum"

msgid "Training"
msgstr "Training"

msgid "Service partner"
msgstr "Servicepartner"

msgid "To the shop"
msgstr "Zum Onlineshop"

msgid "Logout"
msgstr "Abmelden"

# Notfications
msgid "System update"
msgstr "System-Update"

msgid "A database update is mandatory"
msgstr "Ein Datenbank-Update ist zwingend notwendig."

msgid "Data system"
msgstr "Dateisystem"

msgid "Some directories are not writeable."
msgstr "Es sind Verzeichnisse nicht beschreibbar."

msgid "Please delete the installation directory '/install' in the root directory."
msgstr "Bitte löschen Sie das Installationsverzeichnis '/install/' im Wurzelverzeichnis Ihres JTL-Shops."

msgid "Database"
msgstr "Datenbank"

msgid "There are errors within the database structure."
msgstr "Es liegen Fehler in der Datenbankstruktur vor."

msgid "Your template version differs from your shop version.<br />For additional help about template updates go to <i class='fa fa-external-link'></i> Wiki"
msgstr "Ihre Template-Version unterscheidet sich von Ihrer Shop-Version.<br/>Weitere Hilfe zu Template-Anpassungen finden Sie im <i class=\"fa fa-external-link\"></i> JTL-Guide."

msgid "You are using a full responsive template. The activation of a separate mobile template is not necessary in this case."
msgstr "Sie nutzen ein Full-Responsive-Template. Die Aktivierung eines separaten Mobile-Templates ist in diesem Fall nicht notwendig."

msgid "You have activated a non-standard template!"
msgstr "Sie haben kein Standard-Template aktiviert."

msgid "The profiler is active. This can cause strong power loss in the shop."
msgstr "Der Plugin-Profiler ist aktiv. Dies kann zu starken Leistungseinbußen im Onlineshop führen."

msgid "New plugin versions are available."
msgstr "Es sind neue Plugin-Versionen vorhanden."

msgid "Configuration"
msgstr "Konfiguration"

msgid "You've selected Google reCaptcha as spam protection method, but homepage and or secrect key have not been added."
msgstr "Sie haben Google reCaptcha als Spamschutz-Methode gewählt, aber Websiteschlüssel und/oder Geheimen Schlüssel nicht angegeben."

msgid "A coupon being used in a survey is not active or does not exist anylonger."
msgstr "In einer Umfrage wird ein Coupon verwendet, der inaktiv ist oder nicht mehr existiert."

msgid "The full-text index does not exist!"
msgstr "Der Volltextindex ist nicht vorhanden."

msgid "E-mail template broken"
msgstr "E-Mail-Vorlage defekt"

msgid "The E-mail template 'Passwort vergessen'/'Forgotten password' is outdated. The variable $neues_passwort is not available anymore. Please replace this with $passwordResetLink or reset the template."
msgstr "Die E-Mail-Vorlage „Passwort Vergessen“ ist veraltet. Die Variable $neues_passwort ist nicht mehr verfügbar. Bitte ersetzen Sie diese durch $passwordResetLink oder setzen Sie die Vorlage zurück."

msgid "Insecure SMTP connection"
msgstr "Unsichere SMTP-Verbindung"

msgid "You have selected SMTP as mail-method, but no encryption method has been chosen. We recommend you urgently to adjust your mail settings. You can find these options at 'System > E-Mails > Emaileinstellungen > SMTP Security'"
msgstr "Sie haben SMTP gewählt, allerdings keine SMTP-Security-Methode. Wir empfehlen Ihnen dringend, Ihre E-Mail-Einstellungen anzupassen. Sie finden die Option unter „System &gt; E-Mails &gt; E-Mail-Einstellungen &gt; SMTP-Security.“"

msgid "User management"
msgstr "Benutzerverwaltung"

msgid "The algorithm for saving passwords has changed. <br/>Please generate a new emergency code for the 2 factor authentication."
msgstr "Der Algorithmus zur Passwortspeicherung hat sich geändert.<br/>Bitte erzeugen Sie neue Notfall-Codes für die Zwei-Faktor-Authentifizierung."

msgid "My Plugins"
msgstr "Meine Plugins"
