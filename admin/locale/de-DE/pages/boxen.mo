��    W      �     �      �  
   �     �     �     �     �     �     �          &     5     J     W  
   h     s  
   �     �  
   �     �     �     �     �     �     �  	   	     	     	     $	     1	     I	     Q	  	   a	     k	     w	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  	   �	     �	     �	     
     "
     8
     G
     V
     e
     r
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     
               %     3     ?  
   L     W     f     t     �     �     �     �     �     �     �          "     5     J     \     k     ~     �  (   �  
   �     �     �          &     :     M     _     t     �     �     �  
   �     �  
   �     �  
   �  	   �            !   4     V     b  	   p  	   z     �     �     �     �     �  	   �     �     �     �          %     1     :     @     D  �  J          /  �   C  �   �     �     �     �  %   �  !     #   .  �   R  '   �     �     
  
         +     4     D  7   b  <   �     �     �  	   �                     4     ;     [  #   n  (   �  !   �     �  %   �  	   "     ,  	   3  	   =     G     N     V     p     �  �   �  @   2     *   -          4   /      (   N                3   B       $              M       <           G              ?   0          8       ;       	                 K                     Q   &          E                             F          @   T   U   J   )           :   .   %       =   D   S   9       A      !   5   V   ,   7       2                 6   +      O          P   >          L              W      1   H   R         I   '   "             
   #   C                    Bestseller Eigene Box (Mit Rahmen) Eigene Box (Ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Suchspecial) Filter (Tag) Globale Merkmale Hersteller In Kürze Verfügbar Kategorien Konfigurator Linkgruppe Login Neue im Sortiment News (Monatsübersicht) News Kategorien Schnellkauf Sonderangebot Suchwolke Tagwolke Top Angebot Top bewertet Trusted Shops - Reviews Umfrage Vergleichsliste Warenkorb Wunschliste Zuletzt angesehen activateOnAnyPage boxEdit boxLabel boxTemplate boxTitle boxType boxen boxenDesc boxenURL catBoxNum catBoxNumTooltip confirmDeleteBox deactivateOnAnyPage deactivatedOnAllPages deleteSelected errorBoxCreate errorBoxDelete errorBoxEdit errorBoxesVisibilityEdit errorContainerCreate inContainer invisibleBoxes linkgroup new newContainer noBoxActivated noBoxesAvailableFor noTemplateConfig pleaseSelect remove resort sectionBottom sectionLeft sectionRight sectionTop showBoxOnlyFor showContainer successBoxCreate successBoxDelete successBoxEdit successBoxRefresh successContainerCreate templateTypeCategory templateTypeContent templateTypeExtension templateTypeLinkList templateTypePlugin templateTypeTemplate visibleOnAllPages visibleOnPages visibleOnSomePages warningChangesForAllPages warningInvisibleBoxes Content-Type: text/plain; charset=UTF-8
 Bestseller Eigene Box (mit Rahmen) Eigene Box (ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Artikel-Overlay) Filter (Tag) Globale Merkmale Hersteller In Kürze verfügbar Kategorien Konfigurator Linkgruppe Anmeldung Neu im Sortiment News: Monate mit Beiträgen News: Kategorien des News-Systems Schnellkauf Sonderangebot Suchwolke Tag-Wolke Top-Artikel Top bewertet Trusted Shops – Reviews Umfrage Vergleichsliste Warenkorb Wunschzettel Zuletzt angesehen %s auf jeder Seite aktivieren Box bearbeiten Bezeichnung Template Titel Typ Boxen Hier können Sie verschiedenen Boxen-Containern Boxen hinzufügen bzw. Boxen entfernen. Für jeden Container können Sie festlegen, auf welchen Seiten er sichtbar sein soll. Ob ein spezieller Container verfügbar ist oder nicht, wird durch das Template bestimmt. Sie können eigene Boxen, z. B. Linkgruppen oder Boxen mit eigenem xHTML-Inhalt, in einem Boxen-Container anlegen. Einige Boxen können Sie über die Bearbeiten-Schaltfläche weiter konfigurieren. https://jtl-url.de/2uknw Kategoriebox-Nummer Listet nur die Kategorien mit dem Kategorieattribut „kategoriebox“ aus JTL-Wawi und der gesetzten Nummer auf. Standard=0 für alle Kategorien mit oder ohne Funktionsattribut. Sind Sie sicher, dass Sie die Box „%s“ löschen möchten? Wenn Sie einen Container löschen, werden alle im Container enthaltenen Boxen ebenfalls entfernt. %s auf jeder Seite deaktivieren auf allen Seiten deaktiviert Markierte löschen Box konnte nicht hinzugefügt werden. Box konnte nicht entfernt werden. Box konnte nicht bearbeitet werden. Die Sichtbarkeit der Boxen konnte nicht gesetzt werden. Es ist ein MySQL-Fehler aufgetreten. Bitte wenden Sie sich an den Support. Container konnte nicht angelegt werden. In Container Nicht sichtbare Boxen Linkgruppe Neue Box Neuer Container Es wurde keine Box aktiviert. Momentan sind noch keine Boxen für „%s“ vorhanden. Die Konfigurationsdatei (template.xml) wurde nicht gefunden. Bitte auswählen Aus Liste entfernen Sortieren Footer Linke Seitenleiste Rechte Seitenleiste Header Box „%s“ nur anzeigen für: Container anzeigen Box wurde erfolgreich hinzugefügt.  Box(en) wurde(n) erfolgreich gelöscht. Box wurde erfolgreich bearbeitet. Die Boxen wurden aktualisiert. Container wurde erfolgreich angelegt. Kategorie Inhalt Extension Linkliste Plugin Vorlage sichtbar auf allen Seiten Sichtbar auf folgenden Seiten sichtbar auf manchen Seiten Die Auswahl „Alle Seiten“ überschreibt entsprechende Änderungen an den Boxen, die vorher für einzelne Seiten vorgenommen wurden. Es existieren Boxen, die aktuell nicht angezeigt werden können. 