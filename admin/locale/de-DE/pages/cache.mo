��    P      �  k         �  	   �  	   �     �     �     �          *     @     S     c     {     �     �     �     �     �     �          '     @     T     e     {     �     �     �     �     �     �     	     	     -	     @	     G	     Z	     f	  
   l	     w	     	     �	     �	     �	     �	     �	     �	     �	     
     
     *
     3
     ;
     @
     H
  
   X
     c
     j
     v
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          .     I     d     r     {     �  	   �  
   �  
   �     �  (   �  	   �     �     �     
           (  	   ?  &   I     p     v  
   �     �     �     �     �  +   �            
   (  &   3     Z     _     {  !   �     �     �     �     �     �     �          /     N     \     v     �     �  	   �     �  *   �  &   �  A     >   I  1   �  +   �     �  $   �     $     5     G     P     U     ]  
   r     }     �  �   �     b     q     }     �     �     �  	   �     �  )   �       %   $  )   J  +   t  1   �     �  	   �     �     �     �     �          
               7   (      J   #   %   D               8          0   6      =   P       G                     "   2      ,       N   &       /   >   '                         @          O                     C              ;       )      B       <             K   *   +       .   1   
   $          H   -   3          4   9      ?          :   L          I      A       5   M   !       	      F   E    benchmark busySpace cacheURL cg_article_description cg_article_nicename cg_attribute_description cg_attribute_nicename cg_box_description cg_box_nicename cg_category_description cg_category_nicename cg_core_description cg_core_nicename cg_filter_description cg_filter_nicename cg_language_description cg_language_nicename cg_manufacturer_description cg_manufacturer_nicename cg_news_description cg_news_nicename cg_object_description cg_object_nicename cg_option_description cg_option_nicename cg_plugin_description cg_plugin_nicename cg_template_description cg_template_nicename clearObjectCache clearPageCache clearTemplateCache config configurationError description empty emptySpace entries entriesCount errorBenchmark errorCacheDelete errorCacheMethodSelect errorCacheTypeDelete errorDirDelete errorFileDelete errorMethodNotFound errorNoCacheType filesFrontend fullSize hitRate hits inserts keyCountInCache management misses objectcache objectcacheDesc repeats runs scriptCountInCache showAdvanced slowlog startBenchmark stats status successCacheDelete successCacheEmptied successCacheMethodSave successCacheTypeActivate successCacheTypeDeactivate successTemplateCacheDelete templateCache testData time type typeArray typeObject typeString uptime Content-Type: text/plain; charset=UTF-8
 Benchmark Belegter Speicher https://jtl-url.de/lt1ec Enthält Artikeldaten Artikel Enthält Attributdaten Attribute Enthält Boxen und deren Einstellungen Boxen Enthält Kategoriedaten Kategorien Enthält JTL-eigene Daten Core Enthält Filterdaten Artikelfilter Enthält Sprachvariablen und Übersetzungen Sprache Enthält Herstellerdaten Hersteller Enthält Newseinträge und -kategorien News Enthält allgemeine Objekte Objekte Enthält allgemeine Einstellungen Optionen Enthält Plugin-Daten Plugins Enthält Template-Einstellungen Template Gesamten Objekt-Cache leeren Gesamten Seiten-Cache leeren Gesamten Template-Cache leeren Konfiguration (fehlerhaft konfiguriert) Beschreibung leeren Freier Speicher Einträge Anzahl Einträge Benchmark konnte nicht ausgeführt werden. Der Cache konnte nicht geleert werden. Es konnte keine funktionierende Cache-Methode ausgewählt werden. Der Cache %s konnte nicht geleert werden (evtl. bereits leer). Das Verzeichnis %s konnte nicht gelöscht werden! Die Datei %s konnte nicht gelöscht werden! Keine Methoden gefunden. Es wurde kein Cache-Typ ausgewählt. Dateien Frontend Komplette Größe Hit-Rate Hits Inserts Anzahl Keys im Cache Verwaltung Misses Objekt-Cache Hier stellen Sie den von JTL-Shop genutzten Cache ein. Der Objekt-Cache puffert Daten, damit sie schneller geladen werden. Sie können für jede Objektart festlegen, ob diese im Cache gespeichert werden soll. Wiederholungen Durchläufe Anzahl Skripte im Cache Erweiterte Optionen anzeigen Slowlog Benchmark starten Statistik Status Object-Cache wurde erfolgreich gelöscht.  Caches erfolgreich geleert.  wurde als Cache-Methode gespeichert. Ausgewählte Typen erfolgreich aktiviert. Ausgewählte Typen erfolgreich deaktiviert. Es wurden %d Dateien im Template-Cache gelöscht! Template-Cache Testdaten Zeiten Typ Array Objekt String Uptime 