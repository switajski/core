<div id="errorModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{__('error')}</h5>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="errorAlert">
                    {__('somethingHappend')}
                </div>
            </div>
        </div>
    </div>
</div>
